srcs = $(wildcard *.md)
pdfs = $(srcs:.md=.pdf)
htmls = $(srcs:.md=.html)

.SUFFIXES: .md .pdf .html

.md.pdf:
	sp-docgen $< -o $@

.md.html:
	sp-docgen $< -o $@

all: $(pdfs) $(htmls)

contractor.pdf contractor.html: contractor.md funcs.py binds.yaml
