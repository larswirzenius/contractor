#!/bin/sh
#
# Create a new VM using libvirt on the local host.

set -eu


verbose()
{
    echo "INFO: $@"
}


die()
{
    echo "$@" 1>&2
    exit 1
}


# Check parameters.

if [ "$#" -lt 2 ]
then
    die "Usage: $ IMAGE PORT"
fi


# Command line parameters: image file and port number for SSH.
image="$1"
port="$2"
verbose "creating VM from image $image"
verbose "once it's running, log in from another terminal: ssh -p $port manager@localhost"

shift 2

# Does the image exist?
if [ ! -e "$image" ]
then
    echo "$image does not exist" 1>&2
    exit 1
fi

# Start VM.
qemu-system-x86_64 \
    -enable-kvm \
    -m 8192 \
    -drive "file=$image,format=raw,if=virtio" \
    -device virtio-net,netdev=user.0 \
    -netdev "user,id=user.0,hostfwd=tcp::$port-:22"
 
