#!/bin/bash

set -eu -o pipefail

./contractor -c manager-config.yaml build nop.yaml
rm -f ~/contractor.log
