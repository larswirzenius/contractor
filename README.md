# Contractor -- build software (more) securely

The Contractor lets you build software with less worry that it will
do something bad. This is an ugly proof of concept prototype and
certainly not ready for real use.

This will probably only work on Debian 10 (buster) and later. To use,
you need to have libvirt, virt-install, and vmdb2, and several
gigabytes of disk space, RAM, and CPUs, and fair bit of bandwidth.
Possibly other things.

## Use pre-built images

Warning: the uncompressed files are big. You need at least a few tens
of gigabytes of free disk space. If you build your own images, you can
do with less disk space.

* Clone this source repository:  

  `git clone https://gitlab.com/larswirzenius/contractor.git`  
  `cd contractor`

* Download manager and worker VM images and uncompress them:  

  `curl https://files.liw.fi/contractor/vm.img.xz > vm.img.xz`  
  `curl https://files.liw.fi/contractor/worker.img.xz > worker.img.xz`  
  `unxz -v *.img.xz`

* Change the manager to accept your own SSH public. This needs the
  `kpartx` program installed. Change the key
  filename in the command as needed:  

  `sudo sh -x ./set-authorized-key vm.img ~/.ssh/id*.pub`

* Create a workspace disk image:  

  `truncate -s 50G workspace.img`  
  `sudo mkfs -t ext4 workspace.img`

* Create manager VM:  

  `./vm-libvirt.sh contractor vm.img workspace.img`

  This step requires libvirt and virt-installer installed.


## Build everything yourself

[vmdb2]: https://vmdb2.liw.fi/

This step gives you exactly the images you want, but requires having
the [vmdb2][] tool installed.

* Edit Ansible playbook to set `user_pub` to your SSH public key, and
  any other things you want to change such as size of the image:  

  `editor vm.yml`

* Create two VM images (this can take a while):  

  `sudo ./vm.sh`

* Create the outer, manager VM, using libvirt:  

  `./vm-libvirt.sh`

* Alternatively, create it using qemu only (this will run until you
  shut down the VM):  

  `./vm-qemu.sh vm.img 7777`
  
  You need to specify `--manager-address` (or `-m`) and
  `--manager-port` (or `-p) when using contractor.


## Try the Contractor

The following assumses you have a running Contractor.

* Find out what the IP address of the VM is:  

  `less /var/lib/libvirt/dnsmasq/virbr0.status`

* Check that the manager VM is accessibler (change IP to yours):  

  `./contractor -m 192.168.122.63 manager-status`

* Clone the test repo:  

  `git clone git://git.liw.fi/heippa /tmp/heippa`

* Build (change IP to yours; this will take a while to run):  

  `./contractor -v -m 192.168.122.63 -v build heippa.yaml`

  Add a `--log contractor.log` option to have a log file. It sometimes
  helps for debugging. The output from the build commands goes to the
  stdout and stderr of contactor, not the log file.

* Hopefully all went well. You can examine the workspace of the build
  at `/tmp/heippa-workspace` .

Read contractor.md for more information; formatted versions:

* [HTML](https://files.liw.fi/temp/contractor.html)
* [PDF](https://files.liw.fi/temp/contractor.pdf)

If you think this is project is worthwhile, and would like to help,
please get in touch!

---
title: README for Contractor
author: Lars Wirzenius (liw@liw.fi)
...
