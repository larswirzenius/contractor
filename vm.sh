#!/bin/sh

set -eu

build() {
    vmdb2 "$1" --output "$2" --rootfs-tarball vm.tar.gz -v --log vmdb2.log
}

build vm.vmdb vm.img
build worker.vmdb worker.img
build worker-subplot.vmdb worker-subplot.img
build worker-vmdb2.vmdb worker-vmdb2.img
build worker-scap.vmdb worker-scap.img
